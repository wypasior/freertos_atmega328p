#include <tube/tube.h>
#include <tube/uartRecvTask.h>

#include <lardu/pin.h>

#include <FreeRTOS.h>
#include <task.h>

#include <avr/io.h>

//extern "C" void staticTask(const char *name, TaskFunction_t fn, UBaseType_t priority);

void init_led(void)
{
    DDRD |= 0xf2;
    DDRB |= 0x20;
}

void staticTask(const char *name, TaskFunction_t fn, UBaseType_t priority, uint8_t *stack, StaticTask_t &buffer)
{
  xTaskCreateStatic(fn,
      name,
      configMINIMAL_STACK_SIZE + 200,
      NULL,
      priority,
      stack,
      &buffer);
}

extern "C"
{
void setup(void)
{
    pinMode(loadPin, OUTPUT);
    pinMode(clockPin, OUTPUT);
    pinMode(dataPin, OUTPUT);

    digitalWrite(clockPin, LOW);
    digitalWrite(loadPin, LOW);
    digitalWrite(dataPin, LOW);

    onboard_led_enable();
    onboard_led_off();

    uartRecvSetup();
}

void createTasks(void)
{
    static uint8_t        _tubeStack[configMINIMAL_STACK_SIZE + 200];
    static StaticTask_t   _tubeBuffer;

    static uint8_t        _recvStack[configMINIMAL_STACK_SIZE + 200];
    static StaticTask_t   _recvBuffer;


    staticTask("tube", tubeTask, tskIDLE_PRIORITY + 1, _tubeStack, _tubeBuffer);
    staticTask("uartRecv", uartRecvTask, tskIDLE_PRIORITY + 1, _recvStack, _recvBuffer);

    setup_uart(115200u);
}
}
