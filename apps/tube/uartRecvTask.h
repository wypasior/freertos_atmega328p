#include <apps/tube/dispQueue.h>

#include <serial.h>

#include <avr/interrupt.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "tz.h"

#include <math.h>

static QueueHandle_t rxQ;

ISR(USART_RX_vect)
{
  uint8_t       b;
  b = UDR0;
  xQueueSendFromISR(rxQ, (void*)&b, NULL);
}

void uartRecvSetup()
{
    static uint8_t        _rx_q_buffer[128];
    static StaticQueue_t   _rx_q_struct;
    rxQ = xQueueCreateStatic(128, sizeof(uint8_t), _rx_q_buffer, &_rx_q_struct);
    createDispQueue();
}

struct GpsTime
{
    char buf[0xff]{};
    uint8_t bp{};

    uint8_t time[6]{}; // hhmmss
    uint8_t hzh[2]{};
    uint8_t date[8]{}; // DDMMYYYY

    bool valid{};

    TzSyd tzSyd{};

    void gpsData(char c)
    {
        valid = false;
        if (c == '\n')
        {
            buf[bp] = 0;
            // $GNZDA,124725.00,08,05,2018,00,00*75
            if (bp > 12 && strncmp("$GNZDA,", buf, 7) == 0 && buf[7] != ',' && buf[8] != ',' && buf[9] != ',' && buf[10] != ',' && buf[11] != ',' && buf[12] != ',')
            {
                memcpy(&time[0], &buf[7], 6);
                memcpy(&date[0], &buf[17], 2);
                memcpy(&date[2], &buf[20], 2);
                memcpy(&date[4], &buf[23], 4);
                valid = true;
            }
            bp = 0;
        }
        else
            buf[bp++] = c;
    }

    void serialize(char *str, bool &bigDot, uint8_t &dot)
    {
        tzSyd.apply(date, time, hzh);

        str[0] = ' ';
        str[1] = hzh[0];
        str[2] = hzh[1];
        str[3] = '-';
        str[4] = time[2];
        str[5] = time[3];
        str[6] = '-';
        str[7] = time[4];
        str[8] = time[5];

        bigDot = (time[5] - '0') & 0x01;
        valid = false;
    }

};


static void uartRecvTask(void* pvParameters)
{
    static GpsTime gpsTime;
    DispMsg msg;
    uint8_t c;

    while(true) 
    {
        if(xQueueReceive(rxQ, &c, portMAX_DELAY) == pdTRUE)
        {
            // debug
            // vTaskDelay(1000 / portTICK_PERIOD_MS);
            // const char *testStr = "$GNZDA,124725.00,01,04,2018,00,00*75";
            // for (uint8_t i = 0; i < 34; ++i)
            // {
            //     c = testStr[i];
            //     gpsTime.gpsData(c);
            // }
            // c = '\n';
            // /debug

            gpsTime.gpsData(c);
            if (gpsTime.valid)
            {
                gpsTime.serialize(msg.txt, msg.bigDot, msg.dots);
                sendDispQueue(msg);
            }
        }
    }
}

