#pragma once

#include <stdint.h>
#include <string.h>

// max6921 accepts 20 bits of data, they are routed as follows
// 876543210ABCDEFG.000
//
// segment:
//     A
//   F   B
//     G
//   E   C
//     D
//
// tube digits numbering:
// 012345678
// 888888888

class SegmentDisplay
{
    static constexpr uint8_t digitSymbol[]  { 0b11111100, 0b01100000, 0b11011010, 0b11110010, 0b01100110, 0b10110110, 0b10111110, 0b11100000, 0b11111110, 0b11110110 };
    
    static constexpr uint8_t letterSymbol[] {
      0b11101110, 0b00111110, 0b00011010, 0b01111010, 0b10011110, 0b10001110, 0b11110110, // abcdefg
      0b00101110, 0b00100000, 0b01110000, 0b01101110, 0b00011100, 0b10101000, 0b00101010, 0b00111010, 0b11001110, // hijklmnop
      0b11100110, 0b00001010, 0b10110110, 0b00011110, 0b01111100, 0b00111000, 0b01010100, 0b01101110, 0b01110110, 0b11011010 // qrstuvqxyz
    };

    static constexpr uint8_t invalidSymbol {0b01101100};

    static constexpr uint8_t digitToSymbol(uint8_t digit)
    {
        return (digit > 9) ? invalidSymbol : digitSymbol[digit];
    }

    static uint8_t charToSymbol(uint8_t c)
    {
        if (c >= 'a' && c <= 'z')
            return letterSymbol[c - 'a'];
        if (c >= 'A' && c <= 'Z')
            return letterSymbol[c - 'A'];
        if (c >= '0' && c <= '9')
            return digitSymbol[c - '0'];
        switch (c)
        {
            case '<': // 1st half of m
                return 0b11001100;
            case '>': // 2nd half of m
                return 0b11100000;
            case '(':
                return 0b10011100;
            case ')':
                return 0b11110000;
            case '-':
                return 0b00000010;
            case '_':
                return 0b00010000;
            case ' ':
                return 0b00000000;
            default:
                return invalidSymbol;
        }
    }

    uint8_t displ[9] {};
public:
    void setNumber(uint8_t ndigit, uint8_t n)
    {
        displ[ndigit] = (n < 10) ? digitToSymbol(n) : invalidSymbol;
    }
    void setSymbol(uint8_t ndigit, uint8_t s)
    {
        displ[ndigit] = charToSymbol(s);
    }

    void setString(const char *str, int8_t offset = 0)
    {
        int8_t l = strlen(str);
        for (int8_t i = 0; i < 8; ++i)
        {
            char c{' '};
            auto ii = i - offset;
            if (ii >= 0 && ii < l)
                c = str[ii];
            setSymbol(i + 1, c);  
        }
    }

    void setDot(uint8_t ndigit)
    {
        displ[ndigit] |= 0b1;
    }

    void setDots(bool bigDot, uint8_t bitmap)
    {
        if (bigDot)
            setDot(0);
        for (uint8_t i = 1; i < 9; ++i)
        {
            if (bitmap & (0x01 << i))
                setDot(i);
        }
    }

    uint8_t getSymbol(uint8_t n)
    {
        return displ[n];
    }
};

constexpr uint8_t SegmentDisplay::digitSymbol[], SegmentDisplay::letterSymbol[];