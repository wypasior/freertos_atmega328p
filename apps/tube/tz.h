#pragma once

template <typename Derived>
struct Tz
{
    auto & derived()
    {
        return *static_cast<Derived*>(this);
    }

    void apply(uint8_t *date, uint8_t *time, uint8_t *hzh)
    {
        uint8_t day = 10 * (date[0] - '0') + date[1] - '0';
        uint8_t month =  10 * (date[2] - '0') + date[3] - '0';
        uint16_t year = 1000 * (date[4] - '0') + 100 * (date[5] - '0') + 10 * (date[6] - '0') + date[7] - '0';

        uint8_t hour = 10 * (time[0] - '0') + time[1] - '0';

        auto tzOffset = derived().offset(day, month, year, hour);

        hzh[0] = time[0];
        hzh[1] = time[1] + tzOffset;
        while (hzh[1] > '9')
        {
            hzh[1] -= 10;
            hzh[0] += 1;
        }

        if (hzh[0] > '2' || (hzh[0] == '2' && hzh[1] >= '4'))
        {
            uint8_t d = hzh[0] - 2;
            if (hzh[1] < '4')
            {
                d -= 1;
                hzh[1] += 6;
            }
            else
            {
                hzh[1] -= 4;
            }
            hzh[0] = d;
        }
    }
    
    static uint8_t getDayNumber(uint8_t dd, uint8_t mm, uint16_t yy) // 0 - Sunday, 6- Saturday
    { 
        static int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
        yy -= mm < 3;
        return (yy + yy / 4 - yy / 100 + yy / 400 + t[mm-1] + dd) % 7;
    }
};

struct TzUtc: public Tz<TzUtc>
{
    uint8_t offset(uint8_t day, uint8_t month, uint16_t year, uint8_t hour)
    {
        return 0;
    }
};

struct TzSyd: public Tz<TzSyd>
{
    uint8_t offset(uint8_t day, uint8_t month, uint16_t year, uint8_t hour)
    {
        constexpr uint8_t tz_aest = 10;
        constexpr uint8_t tz_aedt = 11;


        uint8_t cmpDay = day;
        uint8_t cmpMonth = month;
        // calc aussie day & month of the switch
        if ((month == 3 && day == 31) || (month == 9 && day == 30))
        {
            cmpDay = 0;
            ++cmpMonth;
        }
        uint8_t tzOffset = tz_aest;

        if (cmpMonth < 4 || cmpMonth > 10)
            tzOffset = tz_aedt;
        else if (cmpMonth == 4 || cmpMonth == 10)
        {
            bool tzFlip = cmpDay >= 7;
            if (!tzFlip)
            {
                auto dow = getDayNumber(day, month, year);
                if (dow == 6)
                    tzFlip = hour >= 16;
                else 
                    tzFlip = dow < cmpDay;

            }
            if ((cmpMonth == 4 && !tzFlip) || (cmpMonth == 10 && tzFlip))
                tzOffset = tz_aedt;
        }
        return tzOffset;
    }
};

struct TzLon: public Tz<TzLon>
{
    uint8_t offset(uint8_t day, uint8_t month, uint16_t year, uint8_t hour)
    {
        constexpr uint8_t tz_gmt = 0;
        constexpr uint8_t tz_bst = 1;

        uint8_t cmpDay = day;
        uint8_t cmpMonth = month;
        uint8_t tzOffset = tz_bst;

        if (cmpMonth < 3 || cmpMonth > 10)
            tzOffset = tz_gmt;
        else if (cmpMonth == 3 || cmpMonth == 10)
        {
            bool tzBefore = cmpDay < 25; // 31 - 7
            if (!tzBefore)
            {
                auto dow = getDayNumber(day, month, year);
                if (dow == 0)
                    tzBefore = hour < 1;
                else 
                    tzBefore = dow >= cmpDay - 24;

            }
            if ((cmpMonth == 3 && tzBefore) || (cmpMonth == 10 && !tzBefore))
                tzOffset = tz_gmt;
        }

        return tzOffset;
    }
};

struct TzCtz: public Tz<TzCtz>
{
    uint8_t offset(uint8_t day, uint8_t month, uint16_t year, uint8_t hour)
    {
        constexpr int8_t tz_ctz = -6;
        constexpr int8_t tz_cdt = 5;

        uint8_t cmpDay = day;
        uint8_t cmpMonth = month;
        uint8_t tzOffset = tz_ctz;

        if (cmpMonth < 3 || cmpMonth > 10)
            tzOffset = tz_ctz;
        else if (cmpMonth == 3 || cmpMonth == 10)
        {
            bool tzBefore = cmpDay < 25; // 31 - 7
            if (!tzBefore)
            {
                auto dow = getDayNumber(day, month, year);
                if (dow == 0)
                    tzBefore = hour < 1;
                else 
                    tzBefore = dow >= cmpDay - 24;

            }
            if ((cmpMonth == 3 && tzBefore) || (cmpMonth == 10 && !tzBefore))
                tzOffset = tz_cdt;
        }

        return tzOffset;
    }
};