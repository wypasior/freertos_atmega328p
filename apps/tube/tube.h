#pragma once

#include <apps/tube/segmentDisplay.h>
#include <apps/tube/dispQueue.h>

#include <lardu/pin.h>

#include <stdint.h>
#include <limits.h>

#include "FreeRTOS.h"
#include "task.h"

#define  loadPin  11
#define  clockPin 12
#define  dataPin  13

#define  touchPin 10

class Tube
{
     uint8_t nSym = 0;
public:
    void showSymbol(SegmentDisplay &displ)
    {
        auto symbol = displ.getSymbol(nSym);
        uint32_t data = ((uint32_t)1 << (11 + nSym)) | (symbol << 3);

        //Set loadPin low
        digitalWrite(loadPin, LOW);
        for (uint8_t i = 0; i < 20; i++)
        {
            // data shifted on rising edge of clock
            digitalWrite(clockPin, LOW);
            //datapin bit to 1 OR 0
            bool state = (data & ((long)1 << i));
            digitalWrite(dataPin, state ? HIGH: LOW);
            //Set clockPin HIGH
            digitalWrite(clockPin, HIGH);
        }
        digitalWrite(loadPin, HIGH);

        if (++nSym > 8)
            nSym = 0;
    }

    bool vsync()
    {
        return nSym == 0;
    }
};

class Button
{
    uint16_t pressCnt {};
    bool rawState{}, longPressConsumed{};
    static constexpr uint16_t Press = 100;
    static constexpr uint16_t LongPress = 2000;
public:
    void eval()
    {
        if (!rawState)
        {
            pressCnt = 0;
            longPressConsumed = false;
        }

        rawState = (digitalRead(touchPin) == HIGH);
        if (rawState)
        {
            if (pressCnt <= LongPress)
                ++pressCnt;
        }
    }

    bool raw()
    {
        return pressCnt > 0;
    }
    bool pressed()
    {
        return !rawState && (pressCnt > Press) && (pressCnt < LongPress);
    }
    bool longPressed()
    {
        bool rv = !longPressConsumed && (pressCnt > LongPress);
        if (rv)
        {
            longPressConsumed = true;
        }
        return rv;
    }
};

void scrollStr(Tube &tube, SegmentDisplay &displ, auto refreshDelay, const char *str)
{
    constexpr uint16_t scrollMs = 80;
    int8_t o = 8;
    uint16_t scrollTimer = 0;

    int8_t l = strlen(str);
    while (o + l >= 0)
    {
        vTaskDelay(refreshDelay);
        if (scrollTimer < scrollMs)
            ++scrollTimer;

        bool vsync = tube.vsync();
        if (vsync)
        {
            if (scrollTimer == scrollMs)
            {
                displ.setString(str, o--);
                scrollTimer = 0;
            }
        }
        tube.showSymbol(displ);
    }
}


void tubeTask(void* pvParameters)
{
    auto delay_2ms {2 / portTICK_PERIOD_MS};

    static Tube tube;
    SegmentDisplay displ;
    Button button;

    DispMsg dispMsg;

    uint16_t idleTicks{};
    
    scrollStr(tube, displ, delay_2ms, "epoch ti<>e solutions");

    while(1)
    {
        vTaskDelay(delay_2ms);
        if (idleTicks < USHRT_MAX)
            ++idleTicks;

        //button.eval();

        if (button.longPressed())
        {
            //t.incTzOffset();
        }

        bool vsync = tube.vsync();
        if (vsync)
        {
            if (recvDispQueue(dispMsg))
            {
                idleTicks = 0;
                if (dispMsg.txt[0])
                    displ.setSymbol(0, dispMsg.txt[0]); 
                displ.setString(dispMsg.txt + 1);
                displ.setDots(dispMsg.bigDot, dispMsg.dots);
            } else if (idleTicks > 1000)
                displ.setString("no sig");
        }

        tube.showSymbol(displ);
    }
}

