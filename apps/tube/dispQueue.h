#pragma once

#include "FreeRTOS.h"
#include "queue.h"

enum class DispMsgType: uint8_t
{
    TIME = 0
};

struct DispMsg
{
    char txt[9]{};
    bool bigDot{};
    uint8_t dots{};
    DispMsgType msgType = DispMsgType::TIME;

    DispMsg()
    {}
};

QueueHandle_t& getQueueH()
{
	static QueueHandle_t xDispMsgQ{};
	return xDispMsgQ;
}

void createDispQueue()
{
    static constexpr size_t QLen = 2;
    static uint8_t        _q_buffer[QLen * sizeof(DispMsg)];
    static StaticQueue_t   _q_struct;
    getQueueH() = xQueueCreateStatic(QLen, sizeof(DispMsg), _q_buffer, &_q_struct);
}

void sendDispQueue(const DispMsg &msg)
{
    xQueueSendToBack(getQueueH(), (void *) &msg, portMAX_DELAY);
}

bool recvDispQueue(DispMsg &msg)
{
    if (xQueueReceive(getQueueH(), &msg, (TickType_t)0))
         return true;
    return false;
}