#include <lardu/pin.h>

#include <FreeRTOS.h>
#include <task.h>

#include <ssd1306/Framebuffer.h>

#include <avr/io.h>

#include <string.h>

#define  sdaPin  11
#define  sclPin 12


void mkTask(const char *name, TaskFunction_t fn, UBaseType_t priority = tskIDLE_PRIORITY)
{
    static uint8_t        _tubeStack[configMINIMAL_STACK_SIZE + 200];
    static StaticTask_t   _tubeBuffer;
    xTaskCreateStatic(fn,
        name,
        configMINIMAL_STACK_SIZE + 200,
        NULL,
        priority,
        _tubeStack,
        &_tubeBuffer);
}

extern "C"
{
void setup(void)
{
    onboard_led_enable();
    onboard_led_off();
}


Framebuffer fb;

static void oledTask(void* pvParameters)
{
    auto delayms{500 / portTICK_PERIOD_MS};


    fb.drawRectangle(2,2,125,61);

    char buff[] = "FUCK THIS SHIT!";
    fb.drawString(5, 5, buff, strlen(buff), 100, TextAlign::LEFT);

    fb.show();


    while(true)
    {
        onboard_led_off();
        //fb.drawFillRectangle(6, 6, 12, 12, 1);
        //fb.show();
        vTaskDelay(delayms);
        onboard_led_on();
        //fb.drawFillRectangle(6, 6, 12, 12, 0);
        //fb.show();
        vTaskDelay(delayms);
    }
}

void createTasks(void)
{
    mkTask("oled", oledTask, tskIDLE_PRIORITY + 1);
}

}
