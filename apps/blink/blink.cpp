#include <avr/io.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "shell.h"
#include "pwm.h"
#include "spi_bus_master.h"
#include "spi_test.h"
#include "i2c_bus_master.h"
#include "baro_reader.h"
#include "adc.h"
#include "adc_reader.h"


//extern "C" void staticTask(const char *name, TaskFunction_t fn, UBaseType_t priority);

void staticTask(const char *name, TaskFunction_t fn, UBaseType_t priority, uint8_t *stack, StaticTask_t &buffer)
{
  xTaskCreateStatic(fn,
      name,
      configMINIMAL_STACK_SIZE + 200,
      NULL,
      priority,
      stack,
      &buffer);
}

void ledTask(void* pvParameters)
{
    while(1)
    {
        PORTB = (PORTB ^ _BV(PB5));
        vTaskDelay(70 / portTICK_PERIOD_MS);

        PORTB = (PORTB ^ _BV(PB5));
        vTaskDelay(200 / portTICK_PERIOD_MS);

        PORTB = (PORTB ^ _BV(PB5));
        vTaskDelay(300 / portTICK_PERIOD_MS);

        PORTB = (PORTB ^ _BV(PB5));
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}

void init_led(void)
{
    DDRD |= 0xf2;
    DDRB |= 0x20;
}

extern "C"
{
void setup(void)
{
    init_led();

    //pwm_init();

    //adc_init(ADCRef_AVCC, ADCPrescaler_DIV_128);
    //spi_bus_master_init();
    //i2c_bus_master_init(0);

    //shell_init();

    //spi_test_init();
    //baro_reader_init();
    //adc_reader_init();
}

void createTasks(void)
{
    static uint8_t        _stack[configMINIMAL_STACK_SIZE + 200];
    static StaticTask_t   _buffer;
    staticTask("blink", ledTask, tskIDLE_PRIORITY + 1, _stack, _buffer);
}
}
