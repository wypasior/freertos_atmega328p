#pragma once

void setup_uart(uint32_t baud)
{


    UBRR0H = 0;
    UBRR0L = 16;
    UCSR0A = _BV(U2X0);
    // Enable  the transmitter. Reciever is disabled.
    UCSR0C = _BV(UCSZ00) | _BV(UCSZ01); 
    UCSR0B = _BV(TXEN0) | _BV(RXEN0) | _BV(RXCIE0);

/*
	uint16_t baud_setting = (F_CPU / (8 * baud)) - 1;

	// try u2x mode first
	UCSR0A      = _BV(U2X0);

	if (((F_CPU == 16000000UL) && (baud == 57600)) || (baud_setting >4095))
	{
		UCSR0A        = 0;
		baud_setting  = (F_CPU / (16 * baud)) - 1;
	}

	UBRR0H = baud_setting >> 8;
	UBRR0L = baud_setting;

	UCSR0C = _BV(UCSZ00) | _BV(UCSZ01);                  // 8 bit data, no parity, 1 stop-bit
	UCSR0B = _BV(RXEN0)  | _BV(TXEN0) | _BV(RXCIE0);
*/

}

