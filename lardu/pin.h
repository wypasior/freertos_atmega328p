#pragma once

#include <avr/io.h>

#define setpin_in(port, pin) 	port &= ~_BV(pin)
#define setpin_out(port, pin)	port |= _BV(pin)

#define setpin(port, pin) 	port |= _BV(pin)
#define clearpin(port, pin)	port &= ~_BV(pin)

inline void onboard_led_enable(void) {
	setpin_out(DDRB, 5);
}

inline void onboard_led_on(void) {
	setpin(PORTB, 5);
}

inline void onboard_led_off(void) {
	clearpin(PORTB, 5);
}

enum pinmode {
	INPUT  = 0,
	OUTPUT = 1
};

enum pinstate {
	LOW = 0,
	HIGH = 1
};

void pinMode(uint8_t pin, enum pinmode mode)
{
	if (pin < 8)
	{	
		if (mode == INPUT)
			setpin_in(DDRD, pin);
		else
			setpin_out(DDRD, pin);
	} else {
		pin = pin - 8;
		if (mode == INPUT)
			setpin_in(DDRB, pin);
		else
			setpin_out(DDRB, pin);
	}
}

void digitalWrite(uint8_t pin, enum pinstate value)
{
	if (pin < 8)
	{	
		if (value == HIGH)
			setpin(PORTD, pin);
		else
			clearpin(PORTD, pin);
	} else {
		pin = pin - 8;
		if (value == HIGH)
			setpin(PORTB, pin);
		else
			clearpin(PORTB, pin);
	}
}

uint8_t digitalRead(uint8_t pin) 
{
	if (pin < 8)
		return (PIND & (1<<pin));
	else {
		pin = pin - 8;
		return (PINB & (1<<pin));
	}
}