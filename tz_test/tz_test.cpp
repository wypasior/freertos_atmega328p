#include <gtest/gtest.h>
#include <apps/tube/tz.h>
#include <iostream>

TEST (Tz, TzUtc) {
	TzUtc tz;
	ASSERT_EQ(tz.getDayNumber(2, 04, 2017), 0u);
	ASSERT_EQ(tz.getDayNumber(1, 10, 2017), 0u);
	ASSERT_EQ(tz.getDayNumber(1, 04, 2018), 0u);
	ASSERT_EQ(tz.getDayNumber(7, 10, 2018), 0u);
	ASSERT_EQ(tz.getDayNumber(7, 04, 2019), 0u);
	ASSERT_EQ(tz.getDayNumber(6, 10, 2019), 0u);
	ASSERT_EQ(tz.getDayNumber(5, 04, 2020), 0u);
	ASSERT_EQ(tz.getDayNumber(4, 10, 2020), 0u);
	ASSERT_EQ(tz.getDayNumber(4, 04, 2021), 0u);
	ASSERT_EQ(tz.getDayNumber(3, 10, 2021), 0u);
}

TEST (Tz, TzSyd) {
	TzSyd tz;

	auto checkDates = [&tz](uint8_t d1, uint8_t m1, uint16_t y, bool spring)
	{
		constexpr uint8_t daysInMonth[] {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

		uint8_t to1 = spring ? 11 : 10;
		uint8_t to2 = spring ? 10 : 11;

		for (uint8_t m = (spring ? 3 : 9); m <= m1; ++m)
			for (uint8_t d = 1; d < (m == m1 ? d1 : daysInMonth[m] + 1); ++d)
			{
				ASSERT_EQ(tz.offset(d, m, y, 1), to1);
				ASSERT_EQ(tz.offset(d, m, y, 15), to1);
				ASSERT_EQ(tz.offset(d, m, y, 16), to1);
				ASSERT_EQ(tz.offset(d, m, y, 23), to1);
			}

		ASSERT_EQ(tz.offset(d1, m1, y, 0), to1);
		ASSERT_EQ(tz.offset(d1, m1, y, 15), to1);
		ASSERT_EQ(tz.offset(d1, m1, y, 16), to2);
		ASSERT_EQ(tz.offset(d1, m1, y, 22), to2);

		for (uint8_t m = m1; m <= m1 + 1; ++m)
			for (uint8_t d = (m == m1 ? d1 + 1 : 1); d < daysInMonth[m] + 1; ++d)
			{
				ASSERT_EQ(tz.offset(d, m, y, 1), to2);
				ASSERT_EQ(tz.offset(d, m, y, 15), to2);
				ASSERT_EQ(tz.offset(d, m, y, 16), to2);
				ASSERT_EQ(tz.offset(d, m, y, 23), to2);
			}
	};

	checkDates(1, 4, 2017, true);
	checkDates(30, 9, 2017, false);
	checkDates(31, 3, 2018, true);
	checkDates(6, 10, 2018, false);
	checkDates(6, 4, 2019, true);
	checkDates(5, 10, 2019, false);
	checkDates(4, 4, 2020, true);
	checkDates(3, 10, 2020, false);
	checkDates(3, 4, 2021, true);
	checkDates(2, 10, 2021, false);
	checkDates(2, 4, 2022, true);
	checkDates(1, 10, 2022, false);
	checkDates(1, 4, 2023, true);
	checkDates(30, 9, 2023, false);
	checkDates(6, 4, 2024, true);
	checkDates(5, 10, 2024, false);
	checkDates(5, 4, 2025, true);
	checkDates(4, 10, 2025, false);
}


TEST (Tz, TzLon) {
	TzLon tz;

	auto checkDates = [&tz](uint8_t d1, uint8_t m1, uint16_t y, bool spring)
	{
		constexpr uint8_t daysInMonth[] {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

		uint8_t to1 = spring ? 0 : 1;
		uint8_t to2 = spring ? 1 : 0;

		for (uint8_t m = (spring ? 3 : 10); m <= m1; ++m)
			for (uint8_t d = 1; d < (m == m1 ? d1 : daysInMonth[m] + 1); ++d)
			{
				ASSERT_EQ(tz.offset(d, m, y, 0), to1);
				ASSERT_EQ(tz.offset(d, m, y, 1), to1);
				ASSERT_EQ(tz.offset(d, m, y, 12), to1);
				ASSERT_EQ(tz.offset(d, m, y, 23), to1);
			}

		ASSERT_EQ(tz.offset(d1, m1, y, 0), to1);
		ASSERT_EQ(tz.offset(d1, m1, y, 1), to2);
		ASSERT_EQ(tz.offset(d1, m1, y, 12), to2);
		ASSERT_EQ(tz.offset(d1, m1, y, 23), to2);

		for (uint8_t m = m1; m <= m1 + 1; ++m)
			for (uint8_t d = (m == m1 ? d1 + 1 : 1); d < daysInMonth[m] + 1; ++d)
			{
				ASSERT_EQ(tz.offset(d, m, y, 0), to2);
				ASSERT_EQ(tz.offset(d, m, y, 1), to2);
				ASSERT_EQ(tz.offset(d, m, y, 11), to2);
				ASSERT_EQ(tz.offset(d, m, y, 23), to2);
			}
	};

	checkDates(26, 3, 2017, true);
	checkDates(29, 10, 2017, false);
	checkDates(25, 3, 2018, true);
	checkDates(28, 10, 2018, false);
	checkDates(31, 3, 2019, true);
	checkDates(27, 10, 2019, false);
	checkDates(29, 3, 2020, true);
	checkDates(25, 10, 2020, false);
	checkDates(28, 3, 2021, true);
	checkDates(31, 10, 2021, false);
	checkDates(27, 3, 2022, true);
	checkDates(30, 10, 2022, false);
	checkDates(26, 3, 2023, true);
	checkDates(29, 10, 2023, false);
	checkDates(31, 3, 2024, true);
	checkDates(27, 10, 2024, false);
	checkDates(30, 3, 2025, true);
	checkDates(26, 10, 2025, false);
}

