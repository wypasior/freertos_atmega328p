CC=avr-gcc
CXX=avr-g++
MCU=atmega328p
F_CPU=16000000UL

FREERTOS_DIR=miniavrfreertos

FREERTOS_SRC  :=  $(FREERTOS_DIR)/queue.c       \
                  $(FREERTOS_DIR)/tasks.c       \
                  $(FREERTOS_DIR)/list.c        \
                  $(FREERTOS_DIR)/hooks.c       \
                  $(FREERTOS_DIR)/timers.c      \
                  $(FREERTOS_DIR)/port.c

SOURCES :=        $(FREERTOS_SRC) \
                    main.c \
                    pwm.c \
#                    shell.c \
                    spi_bus_master.c \
                    tm1638.c \
                    bmp180.c \
                    bmp180_support.c \
                    i2c_bus_master.c \
                    baro_reader.c \
                    adc.c \
                    adc_reader.c \
                    spi_test.c

blink_CXXSOURCES := apps/blink/blink.cpp
tube_CXXSOURCES := apps/tube/tube.cpp
oled_CXXSOURCES := apps/oled/oled.cpp ssd1306/SSD1306.cpp ssd1306/I2C.cpp 

error:
	@echo "Usage: <target> [flash]"
	@exit 2

V = 0
ifeq ($V, 0)
	Q = @
	P = > /dev/null
else
  Q =
  P = 
endif

AVRDUDE = avrdude

AVRDUDE_PROGRAMMER = arduino
AVRDUDE_PORT = /dev/ttyUSB0

AVRDUDE_VERBOSE = -v -v

AVRDUDE_FLAGS = -p $(MCU) -b 115200
AVRDUDE_FLAGS += -P $(AVRDUDE_PORT)
AVRDUDE_FLAGS += -c $(AVRDUDE_PROGRAMMER)

INC_PATH=-I./ -I$(FREERTOS_DIR)

CFLAGS = -mmcu=$(MCU)
CFLAGS += -funsigned-char -funsigned-bitfields
CFLAGS += -DF_CPU=$(F_CPU) 
CFLAGS += $(INC_PATH)
CFLAGS += -O2 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums
CFLAGS += -mrelax -Wall -Wstrict-prototypes
CFLAGS += -std=gnu11 --param=min-pagesize=0
CFLAGS += -MMD -MP -MF .dep/$(@F).d
CFLAGS += -DBMP180_API

CXXFLAGS = -mmcu=$(MCU)
CXXFLAGS += -funsigned-char -funsigned-bitfields -fconcepts
CXXFLAGS += -DF_CPU=$(F_CPU) 
CXXFLAGS += $(INC_PATH) -Iapps
CXXFLAGS += -O2 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums
CXXFLAGS += -mrelax -Wall
CXXFLAGS += -std=gnu++17 --param=min-pagesize=0
CXXFLAGS += -MMD -MP -MF .dep/$(@F).d
CXXFLAGS += -DBMP180_API

LFLAGS = -mmcu=$(MCU)
LFLAGS += -lm

obj/%.o: %.c
	@echo "[CC] $(notdir $<)"
	$Qmkdir -p $(dir $@)
	$Q$(CC) $(CFLAGS) -c -o $@ $<

obj/%.o: %.cpp
	@echo "[CXX] $(notdir $<)"
	$Qmkdir -p $(dir $@)
	$Q$(CXX) $(CXXFLAGS) -c -o $@ $<

clean:
	@echo "Cleaning hex files"
	$Qrm -f *.hex *.bin *.map *.sym *.lss
	@echo "Cleaning Objects"
	$Qrm -fR obj
	$Qrm -fR .dep

-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)

.SECONDEXPANSION:
CXXSOURCES = $(filter-out clean flash, $(MAKECMDGOALS))_CXXSOURCES

$(filter-out clean flash, $(MAKECMDGOALS)): $(addsuffix .hex,$(filter-out clean flash, $(MAKECMDGOALS)))

$(addsuffix .hex,$(filter-out clean flash, $(MAKECMDGOALS))): $(addprefix obj/,$(SOURCES:.c=.o)) $$(addprefix obj/, $$(patsubst %.cpp,%.o,$$($$(CXXSOURCES))))
	@echo "[LD] $(@)"
	$Qavr-gcc $(LFLAGS) -Wl,-Map=$(addprefix obj/, $(@:.hex=.map)),--cref,--gc-sections -o $(addprefix obj/, $(@:.hex=.elf)) $^ 
	$Qavr-objcopy -O ihex -R .eeprom $(addprefix obj/, $(@:.hex=.elf)) $(@)
	$Qavr-objcopy -I ihex $(@) -O binary $(addprefix obj/, $(@:.hex=.bin))
	$Qavr-size --format=berkeley $(addprefix obj/, $(@:.hex=.elf))
	$Qavr-nm -n $(addprefix obj/, $(@:.hex=.elf)) > $(addprefix obj/, $(@:.hex=.sym))
	$Qavr-objdump -h -S $(addprefix obj/, $(@:.hex=.elf)) > $(addprefix obj/, $(@:.hex=.lss))

flash: $(addsuffix .hex,$(filter-out clean flash, $(MAKECMDGOALS)))
	$(AVRDUDE) $(AVRDUDE_FLAGS) -U flash:w:$^ $(AVRDUDE_WRITE_EEPROM)
