#include <avr/io.h>
#include <util/delay.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"


#if configSUPPORT_STATIC_ALLOCATION
/* static memory allocation for the IDLE task */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, configSTACK_DEPTH_TYPE *pulIdleTaskStackSize) {
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
#endif

void setup(void);
void createTasks(void);

void sos(void)
{
    DDRD |= 0xf2;
    DDRB |= 0x20;
    while(1) 
    {
        for (uint8_t i = 0; i < 18; ++i)
        {
            _delay_ms(i == 12 ? 1000 : ((i < 6 || i >= 12) ? 100 : 400));
            PORTB = (PORTB ^ _BV(PB5));
        }
        _delay_ms(1000);
    }
}

int main(void) __attribute__((OS_main));

int main(void)
{
    setup();
    createTasks();

    vTaskStartScheduler();

    // should never reach here
    // display SOS in morse code on the led to signal error
    sos();
    return 0;
}

